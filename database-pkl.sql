-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 26, 2020 at 04:32 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl-web-base`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500'),
(2, 'alwan', '323ccdb3aa77bbce2660a8eaab29b574'),
(3, 'kontol', 'kontol\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` int(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nama`, `nim`, `alamat`) VALUES
(1, 'Abidin', 12345678, 'Jl. XAMPP'),
(2, 'Amer Bidin', 87654321, 'Jl. Anggur Merah');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kartu`
--

CREATE TABLE `tbl_kartu` (
  `id_kartu` int(11) NOT NULL,
  `gmbr_kartu` varchar(20) NOT NULL,
  `judul_kartu` varchar(50) NOT NULL,
  `isi_kartu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kartu`
--

INSERT INTO `tbl_kartu` (`id_kartu`, `gmbr_kartu`, `judul_kartu`, `isi_kartu`) VALUES
(1, '', 'maantap mantap', 'dfsdfsdfsdfsdfsdfsd'),
(2, '', 'eeeeee', 'dfsdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id_soal` int(11) NOT NULL,
  `judul_soal` varchar(255) NOT NULL,
  `isi_soal` varchar(255) NOT NULL,
  `jawaban` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soal`
--

INSERT INTO `tbl_soal` (`id_soal`, `judul_soal`, `isi_soal`, `jawaban`) VALUES
(4, 'oke', '99', '678'),
(5, 'test', 'jingan lu', 'kering ppk'),
(7, 'jmbod', 'asasjaks', 'mwmwk'),
(8, 'memek', 'lu', 'kering');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `nm_lengkap` varchar(255) NOT NULL,
  `no_tlpn` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `nm_lengkap`, `no_tlpn`, `username`, `password`, `level`) VALUES
(8, 'hue@email.com', 'hue mmk', 20202020, 'jembod', 'jembod', 'kartu'),
(9, 'asu@email.com', 'asdjasdlkj', 290292092, 'kdjlkajsdklasjdk', 'maflaf', 'learning'),
(10, 'kntloaa@gmail.com', 'kntlo', 987654321, 'kntloaa', 'kntlo', 'learning'),
(11, 'memek@gmail.com', 'putri memek', 232323, 'putmek', 'putmek', 'kartu'),
(12, 'adaasu@gmail.com', 'ambyar', 817, 'ambyar', 'kontol', 'admin'),
(13, 'sitifitriana1303@yah', 'huaaa', 20202020, 'alwan', 'sdssss', 'learning'),
(14, 'jembud@email.com', 'jembud', 99998888, '99988', '9898989', 'learning'),
(15, 'ghost.chule@yahoo.co', 'malaikat', 999999999, 'izrail', '0000000', 'admin'),
(16, 'swordknightx@gmail.c', 'huaaa', 20202020, 'alwan', 'gggggggg', 'learning');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kartu`
--
ALTER TABLE `tbl_kartu`
  ADD PRIMARY KEY (`id_kartu`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kartu`
--
ALTER TABLE `tbl_kartu`
  MODIFY `id_kartu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
