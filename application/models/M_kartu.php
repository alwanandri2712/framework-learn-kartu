<?php
class M_kartu extends CI_Model{

  //   \/ function GET kart
  function get_kartu(){
    $kartu = $this->db->get('tbl_kartu')->result();
    return $kartu;
  }

  //   \/ function ADD kartu
  function tambah_kartu($data){
    // tambah gambar
    // tambah teks
    
    $query = $this->db->insert('tbl_kartu',$data);
		if($query){
      return true;
    }else{
      return false;
    }

  }

  //   \/ function EDIT kartu
  function edit_data($where, $table){

    return $this->db->get_where($table,$where);
  }

  //   \/ function UPDATE kartu
  function update_kartu($where,$data,$table){

    $this->db->where($where);
    $this->db->update($table,$data);
  }

  //   \/ function DELETE kartu
  function hapus_kartu($where,$table){

    $this->db->where($where);
    $this->db->delete($table);
  }

}
?>
