<?php

class M_home extends CI_Model{
	
	function get_user(){
		return $this->db->get('user');	
	}

	function get_kartu(){
		$get_kartu = $this->db->get("tbl_kartu");
		return $get_kartu->result_array();
	}
	
	function total_user(){
		$user = $this->db->get("user")->num_rows();
		return $user;
	}

	function tot_admin(){
		$admin = $this->db->get_where('user',['level' => 'admin'])->num_rows();
		return $admin;
	}

	function tot_learning(){
		$learning = $this->db->query("select * from user where level='learning'")->num_rows();
		return $learning;
	}

	function tot_kartu(){
		$kartu = $this->db->get_where('user',['level'=> 'kartu'])->num_rows();
		return $kartu;
	}
}

?>