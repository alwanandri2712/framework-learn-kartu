<?php 
class M_learning extends CI_Model{

	function get_soal(){
		$soal = $this->db->get('tbl_soal')->result();
		return $soal;
	}
	function tambah_soal($data){

		 return $this->db->insert('tbl_soal',$data);

	}

	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_soal($where,$data,$table){

		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function hapus_data($where,$table){

		$this->db->where($where);
		$this->db->delete($table);
	}

}


 ?>
