<?php
class Learning extends CI_controller{

    function __construct() {
        parent::__construct();
		$this->load->model('M_users');
		$this->load->helper('url');
    }

    function index(){
        $this->load->view('v_learning');
    }

    function tambah_proses(){
		$nama = $this->input->post('nm_lengkap');
		$email = $this->input->post('email');
		$no_tlpn = $this->input->post('no_tlpn');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$data = array(
			'nm_lengkap' => $nama,
			'email' => $email,
			'no_tlpn' => $no_tlpn,
			'username' => $username,
			'password' => $password,
			'level' => $level
			);

		$this->M_users->tambah_soal($data);
		redirect('admin/soal');
	}
}

?>
