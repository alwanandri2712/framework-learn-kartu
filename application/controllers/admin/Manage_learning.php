<?php 
/**
 * 
 */
class Manage_learning extends CI_Controller
{	
	function __construct() {
        parent::__construct();
        $this->load->model('M_learning');
    }
	
	function index(){
		$data['learning'] = $this->M_learning->get_soal();
		$this->load->view('admin/v_manage_learning',$data);
	}

	function tambah_soal(){
		$id_soal = $this->input->post('id_soal');
		$judul_soal = $this->input->post('judul_soal');
		$isi_soal = $this->input->post('isi_soal');
		$jawaban = $this->input->post('jawaban');
		// $password = $this->input->post('password');
		// $level = $this->input->post('level');

		$data = array(
			'id_soal' => $id_soal,
			'judul_soal' => $judul_soal,
			'isi_soal' => $isi_soal,
			'jawaban' => $jawaban,
			// 'password' => $password,
			// 'level' => $level
			);

		$this->M_learning->tambah_soal($data);
		redirect('admin/Manage_learning');
	}


	function edit($id){
		$where = array('id_soal' => $id);
		$data['tbl_soal'] = $this->M_learning->edit_data($where,'tbl_soal')->result();
		$this->load->view('admin/v_edit_manage_learning',$data);
	}

	function update_proses(){
		$id_soal = $this->input->post('id_soal');
		$judul_soal = $this->input->post('judul_soal');
		$isi_soal = $this->input->post('isi_soal');
		$jawaban = $this->input->post('jawaban');
		// $username = $this->input->post('username');
		// $password = $this->input->post('password');
		// $level = $this->input->post('level');

		$data = array(
			'id_soal' => $id_soal,
			'judul_soal' => $judul_soal,
			'isi_soal' => $isi_soal,
			'jawaban' => $jawaban,
			// 'password' => $password,
			// 'level' => $level
			);

		$where = array(
			'id_soal' => $id_soal
		);

		$this->M_learning->update_soal($where,$data,'tbl_soal');
		redirect('admin/Manage_learning');
	}

	function hapus_users($id){
		$where = array('id_soal' => $id);
		$this->M_learning->hapus_data($where,'tbl_soal');
		redirect('admin/Manage_learning');
	}
}
 ?>