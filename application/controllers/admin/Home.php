<?php
class Home extends CI_controller{

	function __construct() {
        parent::__construct();
        $this->load->model('m_home');
    }

    function index(){
    	$z 	['total_user'] 	   = $this->m_home->total_user();
    	$z 	['total_learning'] = $this->m_home->tot_learning();
    	$z 	['total_kartu']	   = $this->m_home->tot_kartu();
    	$z	['total_admin']    = $this->m_home->tot_admin();
        $this->load->view('admin/v_home',$z);
    }
}

?>
