<?php
/**
 *
 */
class Users extends CI_Controller
{
	function __construct() {
        parent::__construct();
		$this->load->model('M_users');
		$this->load->helper('url');
    }

	function index(){
		$data['user'] = $this->M_users->get_users();
		$this->load->view('admin/v_users',$data);
	}

	function tambah_proses(){
		$nama = $this->input->post('nm_lengkap');
		$email = $this->input->post('email');
		$no_tlpn = $this->input->post('no_tlpn');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$data = array(
			'nm_lengkap' => $nama,
			'email' => $email,
			'no_tlpn' => $no_tlpn,
			'username' => $username,
			'password' => $password,
			'level' => $level
			);

		$this->M_users->tambah_users($data);
		redirect('admin/users');
	}


	function edit($id){
		$where = array('id' => $id);
		$data['user'] = $this->M_users->edit_data($where,'user')->result();
		$this->load->view('admin/v_edit_users',$data);
	}

	function update_proses(){
		$id = $this->input->post('id');
		$nm_lengkap = $this->input->post('nm_lengkap');
		$email = $this->input->post('email');
		$no_tlpn = $this->input->post('no_tlpn');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$data = array(
			'email' => $email,
			'nm_lengkap' => $nm_lengkap,
			'no_tlpn' => $no_tlpn,
			'username' => $username,
			'password' => $password,
			'level' => $level
			);

		$where = array(
			'id' => $id
		);

		$this->M_users->update_users($where,$data,'user');
		redirect('admin/users');
	}

	function hapus_users($id){
		$where = array('id' => $id);
		$this->M_users->hapus_data($where,'user');
		redirect('admin/users');
	}
}
 ?>
