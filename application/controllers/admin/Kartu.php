<?php

class Kartu extends CI_controller
{
	function __construct() {
        parent::__construct();
		$this->load->model('M_kartu');
		$this->load->helper(array('form', 'url'));
   }

  	function index(){
		$z ['kartu'] = $this->M_kartu->get_kartu();
		$this->load->view('admin/v_kartu',$z);
  	}


	function tambah_proses(){
		$config['upload_path']      = './assets/img';
		$config['allowed_types']    = 'jpg|png';
		// $config['encrypt_name'] = TRUE;
		$config['max_size']         = 2048;
		$config['remove_space']			= TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){

			echo "gagal";

		}else{

			$gambar = $this->upload->data('file_name');
			// $gambar = $people['file_name'];

			$data = array(
				// 'id_kartu' => $this->input->post('id_kartu'),
				'judul_kartu' => $this->input->post('judul_kartu'),
				'isi_kartu' => $this->input->post('isi_kartu'),
				'gmbr_kartu' => $gambar
			);
			$this->db->insert('tbl_kartu', $data);
			redirect('admin/kartu');
		}
	}

	function edit($id_kartu){
		$where = array('id_kartu' => $id_kartu);
		$data['kartu'] = $this->M_kartu->edit_data($where,'tbl_kartu')->result();
		$this->load->view('admin/v_edit_kartu',$data);
	}


	function update_proses(){
		$config['upload_path']          = './assets/img';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 2048;
		$config['remove_space']			= TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){

			$error = array('error' => $this->upload->display_errors());
			print_r($error);die();  

		}else{

			$gambar = $this->upload->data('file_name');

			$id_kartu = $this->input->post('id_kartu');
			$judul_kartu = $this->input->post('judul_kartu');
			$kata_kunci = $this->input->post('isi_kartu');

			$data = array(
				'gmbr_kartu' => $gambar,
				'judul_kartu' => $judul_kartu,
				'isi_kartu' => $kata_kunci,
				);

			$where = array(
				'id_kartu' => $id_kartu
			);

			$this->M_kartu->update_kartu($where,$data,'tbl_kartu');
			redirect('admin/kartu');
		}
	}

	function hapus_kartu($id){
		$where = array('id_kartu' => $id);
		$this->M_kartu->hapus_kartu($where,'tbl_kartu');
		redirect('admin/kartu');
	}

}


?>
