<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Soal Kartu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->                
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/custom.css">
    <link rel="shortcut icon" href="<?= base_url()?>assets/img/favicon.png?3">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page-holder d-flex align-items-center">
      <div class="container">
        <div class="row align-items-center py-5">
          <div class="col-5 col-lg-5 mx-auto mb-7 mb-lg-0">
            <div class="pr-lg-5"><img src="<?= base_url()?>assets/img/vector/soal1.svg" alt="" class="img-fluid"></div>
          </div>
          <div class="col-lg-5 px-lg-4">
            <h2 class="mb-4">Jawaban</h2>
            <p class="text-muted">Jawablah soal berikut dengan kalimat yang tepat</p>
            <form id="loginForm" action="soal_kartu.php" method="post" class="mt-4">
              <div class="form-group mb-4">
                <input type="text" name="kalimat" placeholder="Tuliskan Kalimat" class="form-control border-0 shadow form-control-lg">
              </div>
              <div class="form-group mb-4">
              </div>
              <button type="submit" class="btn btn-primary shadow px-5">Check</button>
            </form>
          </div>
        </div>
        <p class="mt-5 mb-0 text-gray-400 text-center" font-color:black>Design by <a href="https://www.gataungoding.id/" class="external text-gray-400">PKL-TEAM</a></p>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="<?= base_url()?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?= base_url()?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>assets/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?= base_url()?>assets/vendor/chart.js/Chart.min.js"></script>
    <script src="<?= base_url()?>https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="<?= base_url()?>assets/js/front.js"></script>
  </body>
</html>