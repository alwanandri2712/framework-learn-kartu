<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Soal Learning</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css')?>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->                
    <link rel="stylesheet" href="<?=base_url('https://fonts.googleapis.com/css?family=Poppins:300,400,800')?>">
    <!-- chevron right -->
    <link rel="stylesheet" href="<?= base_url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');?>">
    <!-- orion icons-->
    <link rel="stylesheet" href="<?=base_url('assets/css/orionicons.css')?>">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=base_url('assets/css/style.default.css')?>" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=base_url('assets/scss/custom.css')?>">
    <link rel="shortcut icon" href="<?=base_url('assets/img/favicon.png?3')?>">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body> 
    
  <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="admin/home" class="navbar-brand font-weight-bold text-uppercase text-base">Welocome to Learning</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">
            <form id="searchForm" class="ml-auto d-none d-lg-block">
              <div class="form-group position-relative mb-0">
                <button type="submit" style="top: -3px; left: 0;" class="position-absolute bg-white border-0 p-0"><i class="o-search-magnify-1 text-gray text-lg"></i></button>
                <input type="search" placeholder="Search ..." class="form-control form-control-sm border-0 no-shadow pl-4">
              </div>
            </form>
          </li>
          <li class="nav-item dropdown mr-3"><a id="notifications" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle text-gray-400 px-1"><i class="fa fa-bell"></i><span class="notification-icon"></span></a>
            <div aria-labelledby="notifications" class="dropdown-menu"><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a><a href="#" class="dropdown-item"> 
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-green text-white"><i class="fas fa-envelope"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 6 new messages</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-blue text-white"><i class="fas fa-upload"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">Server rebooted</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item text-center"><small class="font-weight-bold headings-font-family text-uppercase">View all notifications</small></a>
            </div>
          </li>
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src=" <?= base_url ('assets/img/avatar-6.jpg')?>" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family">Mark Stephen</strong><small>Web Developer</small></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Settings</a><a href="#" class="dropdown-item">Activity log</a>
              <div class="dropdown-divider"></div><a href="login.html" class="dropdown-item">Logout</a>
            </div>
          </li>
        </ul>
      </nav>
    </header>

   
 <br>
 
   <style>
     ul {
       list-style-type: none;
       margin: 0px;
       padding: 0px;
       overflow: hidden;
       margin-left: 34px;
     }
     li a {
       display: block;
       text-decoration: none;
     }
     li {
       float: left;
     }

     </style>

  <!-- DAY 1 -->
  <ul>
<li>
      <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 1
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      <br>
    </div>
</li>
<!-- END DAY 1 -->

<!-- DAY 2 -->
<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 2
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>
<!-- END DAY 2 -->
   
<!-- DAY 3 -->
<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 3
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      <br>
  </div>
</li>
<!-- END DAY 3 -->

<!-- DAY 4 -->
<li>
      <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 4
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      <br>
    </div>
</li>
<!-- END DAY 4 -->

<!-- DAY 5 -->
<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 5
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>
<!-- END DAY 5 -->
   
<!-- DAY 6 -->
<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 6
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      
      <br>
  </div>
</li>
<!-- END DAY 6 -->
<!-- DAY 7 -->
<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 7
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      </div>
      <br>
  </div>
</li>

<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 8
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      </div>
      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 9
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 10
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      
      <br>
  </div>
</li>

<li>
<div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
      day 11
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 12
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 13
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
    day 14
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: -100%;  margin-top: 48px;">
    day 15
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: -74%; margin-top: 48px;">
    day 16
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: -47%; margin-top: 48px;">
    day 17
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: -19%; margin-top: 48px;">
    day 18
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 10%; margin-top: 48px;">
    day 19
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 37%; margin-top: 48px;">
    day 20
   
    <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

<li>
    <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 64%; margin-top: 48px;">
    day 21
   <a href="Soallearning"   class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

      <br>
  </div>
</li>

</ul>
<!-- END DAY 7 -->



<script type="text/javascript">
function handleSelect(elm){
  window.location = elm.value+".php";
}
</script>
  

    <!-- JavaScript files-->
    <script src="<?= base_url ('assets/vendor/jquery/jquery.min.js')?>"></script>
    <script src="<?= base_url ('assets/vendor/popper.js/umd/popper.min.js')?>"> </script>
    <script src="<?= base_url ('assets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?= base_url ('assets/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
    <script src="<?= base_url ('assets/vendor/chart.js/Chart.min.js')?>"></script>
    <script src="<?= base_url ('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js')?>"></script>
    <script src="<?= base_url ('assets/js/front.js')?>"></script>
  </body>
</html>