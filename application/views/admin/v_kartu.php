
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <title>Manage Kartussgit</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css'?>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/orionicons.css'?>">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.default.css'?>" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/custom.css'?>">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/img/favicon.png'?>">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">

          </li>

            <div aria-labelledby="notifications" class="dropdown-menu"><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-green text-white"><i class="fas fa-envelope"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 6 new messages</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-blue text-white"><i class="fas fa-upload"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">Server rebooted</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item text-center"><small class="font-weight-bold headings-font-family text-uppercase">View all notifications</small></a>
            </div>
          </li>
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="img/avatar-6.jpg" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family">Mark Stephen</strong><small>Web Developer</small></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Settings</a><a href="#" class="dropdown-item">Activity log       </a>
              <div class="dropdown-divider"></div><a href="login.html" class="dropdown-item">Logout</a>
            </div>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">


      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="<?= base_url()?>admin/home" class="sidebar-link text-muted ">
                  <i class="o-home-1 mr-3 text-gray"></i>
                    <span>Home</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_learning" class="sidebar-link text-muted">
                  <i class="o-table-content-1 mr-3 text-gray"></i>
                    <span>Manage Learning</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="<?= base_url()?>admin/kartu" class="sidebar-link text-muted">
                  <i class="fa fa-id-card mr-3 text-gray"></i>
                  <span>Manage Cards</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="<?= base_url()?>admin/users" class="sidebar-link text-muted">
                  <i class="fa fa-users mr-3 text-gray"></i>
                  <span>Manage Users</span>
                </a>
              </li>
              <li class="sidebar-list-item"><a href="login.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>

      </div>


      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
                <div class="card">
                  <div class="card-header">
                    <h6 class="text-uppercase mb-0">Manage Kartu<a class="float-sm-right btn-sm btn-success" data-toggle="modal" data-target="#myModal" style='border-radius:2px;'><i class="fas fa-plus" style='color:white;'></i></a></h6>
                  </div>
                  <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                      <div role="document" class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 id="exampleModalLabel" class="modal-title">Tambah Kartu</h4>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                          </div>
                          <?php echo form_open_multipart('admin/kartu/tambah_proses'); ?>
                          <form method="post">
                          <div class="modal-body">
                              <div class="form-group">
                              <label>Judul Kartu</label>

                                <input type="text" placeholder="Matahari Pagi ..." name='judul_kartu' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Kata Kunci</label>
                                <input type="text" placeholder="Cerah ..." name='isi_kartu' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Gambar kartu</label>
                                <input type="hidden" name="id_kartu">
                                <input type="file" name="userfile">
                              </div>
                            </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                            <button type="submit" class="btn btn-primary" value="upload">Simpan</button>
                          </div>
                          </form>
                          <?php echo form_close(); ?>
                        </div>
                      </div>
                    </div>

                  <div class="card-body" >
                    <div class="table-responsive">
                    	<table class="table">
		                  <thead>
		                    <tr>
		                      <th>No</th>
		                      <th>Gambar</th>
		                      <th class='text-nowrap'>Judul Kartu</th>
		                      <th class='text-nowrap'>Kata Kunci</th>
		                      <th class="text-nowrap" style="text-align: center;">Action</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                  <?php
		                  	$no = 1;
		                    foreach($kartu as $i):
		              		?>
		                 <tr>
		                  	<td><?php echo $no++ ?></td>
		                    <td><img style='width:50px;' src="<?= base_url();?>assets/img/<?= $i->gmbr_kartu;?>"></td>
		                    <td><?php echo $i->judul_kartu;?></td>
		                    <td><?php echo $i->isi_kartu;?></td>
                        <td>
                          <?php
                          echo"
                          <a class='btn btn-info btn-sm text-nowrap'
                            href='kartu/edit/$i->id_kartu'>
                            <i class='fas fa-edit'></i>
                          </a>";
                          ?>
                          <?php
                          echo"
                          <a class='btn btn-danger btn-sm text-nowrap'
                            href='kartu/hapus_kartu/$i->id_kartu'
                            onclick='return confirm(`Yakin Delete?`)'>
                            <i class='fas fa-trash'></i>
                          </a>";
                          ?>
                        </td>
		             	</tr>
		               	<?php endforeach;?>
		                  </tbody>
	                </table>
                  </div>
                </div>
            </div>
          </section>
        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Intinya Alwan Ganteng &copy;2020</p>
              </div>

            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="<?php echo base_url().'assets/vendor/jquery/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/vendor/popper.js/umd/popper.min.js'?>"> </script>
    <script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/vendor/jquery.cookie/jquery.cookie.js'?>"> </script>
    <script src="<?php echo base_url().'assets/vendor/chart.js/Chart.min.js'?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="<?php echo base_url().'assets/js/charts-home.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/front.js';?>"></script>
  </body>
</html>
