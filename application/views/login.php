<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LOGIN </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css')?>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href=" <?= base_url ('https://use.fontawesome.com/releases/v5.3.1/css/all.css')?>" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href=" <?= base_url ('https://fonts.googleapis.com/css?family=Poppins:300,400,800')?>">
    <!-- orion icons-->
    <link rel="stylesheet" href="<?= base_url('assets/css/orionicons.css')?>">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.default.css')?>" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?= base_url('assets/css/custom.css')?>">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png')?>">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page-holder d-flex align-items-center">
      <div class="container">
        <div class="row align-items-center py-5">
          <div class="col-5 col-lg-7 mx-auto mb-5 mb-lg-0">
            <div class="pr-lg-5"><img src="<?= base_url('assets/img/illustration.svg')?>" alt="" class="img-fluid"></div>
          </div>
          <div class="col-lg-5 px-lg-4">
            <h2 class="mb-4">Login</h2>
            <p class="text-muted">Pembelajaran Onlen</p>
            <form id="loginForm" action="proses_login.php" method="post" class="mt-4">
              <div class="form-group mb-4">
                <input type="text" name="username" placeholder="Username" class="form-control border-0 shadow form-control-lg">
              </div>
              <div class="form-group mb-4">
                <input type="password" name="password" placeholder="Password" class="form-control border-0 shadow form-control-lg text-violet">
              </div>
              <div class="form-group mb-4">
                <div class="custom-control custom-checkbox">
                  <input id="customCheck1" type="checkbox" checked class="custom-control-input">
                  <label for="customCheck1" class="custom-control-label">Remember Me</label>
                  <a href='daftar.php'><span style='position:relative;left:180px;'>Silahkan Daftar</span></a>
                </div>
              </div>
              <button type="submit" class="btn btn-primary shadow px-5">Sign in</button>
            </form>
          </div>
        </div>
        <p class="mt-5 mb-0 text-gray-400 text-center" font-color:black>Design by <a href="https://www.gataungoding.id/" class="external text-gray-400">PKL-TEAM</a></p>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="<?= base_url('assets/vendor/popper.js/umd/popper.min.js')?>"> </script>
    <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?= base_url('assets/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
    <script src="<?= base_url('assets/vendor/chart.js/Chart.min.js')?>"></script>
    <script src="<?= base_url ('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js')?>"></script>
    <script src="<?= base_url ('js/front.js')?>"></script>
  </body>
</html>
