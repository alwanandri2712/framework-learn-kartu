<div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="index.php" class="sidebar-link text-muted active">
                  <i class="o-home-1 mr-3 text-gray"></i>
                    <span>Home</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_learning.php" class="sidebar-link text-muted">
                  <i class="o-table-content-1 mr-3 text-gray"></i>
                    <span>Manage Learning</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_kartu.php" class="sidebar-link text-muted">
                  <i class="fa fa-id-card mr-3 text-gray"></i>
                  <span>Manage Kartu</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_users.php" class="sidebar-link text-muted">
                  <i class="fa fa-users mr-3 text-gray"></i>
                  <span>Manage Users</span>
                </a>
              </li>
              <li class="sidebar-list-item"><a href="login.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>
        
      </div>